VazPac a Win32 game by philvaz

VazPac is a classic 'pacman' type 2D game written in C/C++ for Win32 using full screen GDI graphics.

All resources needed to compile the game are included in this repository.

It was originally compiled using Visual C++ 6.0 but has been upgraded to Visual Studio (Community) 2015.

For more see www.VazGames.com
